package ru.lamelobster.llp.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * Пример DTO.
 */
@Data
@AllArgsConstructor
public class ExampleDTO implements Serializable {
    private Long id;
}
